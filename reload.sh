#!/bin/bash

# Thanks, lloeki. See LICENSE.lloeki.

DRIVER=logiwheel2000

sync
sudo kextunload /tmp/"$DRIVER.kext" 2>&1 >/dev/null;
sudo kextunload /System/Library/Extensions/"$DRIVER.kext" 2>&1 >/dev/null;
sudo rm -rf /System/Library/Extensions/"$DRIVER.kext"
sleep 1;
sudo cp -R "$HOME"/Library/Developer/Xcode/DerivedData/"$DRIVER"-*/Build/Products/Debug/"$DRIVER.kext" /System/Library/Extensions/ &&
sudo kextload /System/Library/Extensions/"$DRIVER.kext"
