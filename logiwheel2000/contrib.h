/*
 * Copyright (c) 2016 Andrew Eikum <coldpie@fastmail.com>
 *
 *   From <linux/drivers/hid/hid-lg4ff.c>:
 * Copyright (c) 2010 Simon Wood <simon@mungewell.org>
 *
 *   From <linux/drivers/hid/hid-lg.c>:
 * Copyright (c) 1999 Andreas Gal
 * Copyright (c) 2000-2005 Vojtech Pavlik <vojtech@suse.cz>
 * Copyright (c) 2005 Michael Haboustak <mike-@cinci.rr.com> for Concept2, Inc
 * Copyright (c) 2006-2007 Jiri Kosina
 * Copyright (c) 2008 Jiri Slaby
 * Copyright (c) 2010 Hendrik Iben
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "logiwheel2000.hpp"

#define DEBUG 1
#define LOGPFX "logiwheel2000: "

#define LG4FF_MMODE_IS_MULTIMODE 0
#define LG4FF_MMODE_SWITCHED 1
#define LG4FF_MMODE_NOT_MULTIMODE 2

#define USB_VENDOR_ID_LOGITECH		0x046d
#define USB_DEVICE_ID_LOGITECH_AUDIOHUB 0x0a0e
#define USB_DEVICE_ID_LOGITECH_T651	0xb00c
#define USB_DEVICE_ID_LOGITECH_C077	0xc007
#define USB_DEVICE_ID_LOGITECH_RECEIVER	0xc101
#define USB_DEVICE_ID_LOGITECH_HARMONY_FIRST  0xc110
#define USB_DEVICE_ID_LOGITECH_HARMONY_LAST 0xc14f
#define USB_DEVICE_ID_LOGITECH_HARMONY_PS3 0x0306
#define USB_DEVICE_ID_LOGITECH_KEYBOARD_G710_PLUS 0xc24d
#define USB_DEVICE_ID_LOGITECH_MOUSE_C01A	0xc01a
#define USB_DEVICE_ID_LOGITECH_MOUSE_C05A	0xc05a
#define USB_DEVICE_ID_LOGITECH_MOUSE_C06A	0xc06a
#define USB_DEVICE_ID_LOGITECH_RUMBLEPAD_CORD	0xc20a
#define USB_DEVICE_ID_LOGITECH_RUMBLEPAD	0xc211
#define USB_DEVICE_ID_LOGITECH_EXTREME_3D	0xc215
#define USB_DEVICE_ID_LOGITECH_DUAL_ACTION	0xc216
#define USB_DEVICE_ID_LOGITECH_RUMBLEPAD2	0xc218
#define USB_DEVICE_ID_LOGITECH_RUMBLEPAD2_2	0xc219
#define USB_DEVICE_ID_LOGITECH_G29_WHEEL	0xc24f
#define USB_DEVICE_ID_LOGITECH_G920_WHEEL	0xc262
#define USB_DEVICE_ID_LOGITECH_WINGMAN_F3D	0xc283
#define USB_DEVICE_ID_LOGITECH_FORCE3D_PRO	0xc286
#define USB_DEVICE_ID_LOGITECH_FLIGHT_SYSTEM_G940	0xc287
#define USB_DEVICE_ID_LOGITECH_WINGMAN_FFG	0xc293
#define USB_DEVICE_ID_LOGITECH_WHEEL	0xc294
#define USB_DEVICE_ID_LOGITECH_MOMO_WHEEL	0xc295
#define USB_DEVICE_ID_LOGITECH_DFP_WHEEL	0xc298
#define USB_DEVICE_ID_LOGITECH_G25_WHEEL	0xc299
#define USB_DEVICE_ID_LOGITECH_DFGT_WHEEL	0xc29a
#define USB_DEVICE_ID_LOGITECH_G27_WHEEL	0xc29b
#define USB_DEVICE_ID_LOGITECH_WII_WHEEL	0xc29c
#define USB_DEVICE_ID_LOGITECH_ELITE_KBD	0xc30a
#define USB_DEVICE_ID_S510_RECEIVER	0xc50c
#define USB_DEVICE_ID_S510_RECEIVER_2	0xc517
#define USB_DEVICE_ID_LOGITECH_CORDLESS_DESKTOP_LX500	0xc512
#define USB_DEVICE_ID_MX3000_RECEIVER	0xc513
#define USB_DEVICE_ID_LOGITECH_UNIFYING_RECEIVER	0xc52b
#define USB_DEVICE_ID_LOGITECH_UNIFYING_RECEIVER_2	0xc532
#define USB_DEVICE_ID_SPACETRAVELLER	0xc623
#define USB_DEVICE_ID_SPACENAVIGATOR	0xc626
#define USB_DEVICE_ID_DINOVO_DESKTOP	0xc704
#define USB_DEVICE_ID_DINOVO_EDGE	0xc714
#define USB_DEVICE_ID_DINOVO_MINI	0xc71f
#define USB_DEVICE_ID_LOGITECH_MOMO_WHEEL2	0xca03
#define USB_DEVICE_ID_LOGITECH_VIBRATION_WHEEL	0xca04

int lg4ff_init(com_smokingonabike_driver_logiwheel2000 *hid);
