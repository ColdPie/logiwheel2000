/*
 * Copyright (c) 2016 Andrew Eikum <coldpie@fastmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <IOKit/IOLib.h>

#include "contrib.h"
 
// This required macro defines the class's constructors, destructors,
// and several other methods I/O Kit requires.
OSDefineMetaClassAndStructors(com_smokingonabike_driver_logiwheel2000, IOUSBHostHIDDevice)

// Define the driver's superclass.
#define super IOUSBHostHIDDevice
 
bool com_smokingonabike_driver_logiwheel2000::start(IOService *provider)
{
    IOLog(LOGPFX "Starting\n");

    if(!super::start(provider)){
        IOLog(LOGPFX "super.start failed\n");
        return false;
    }

    lg4ff_init(this);

    return true;
}
