/*
 * Copyright (c) 2016 Andrew Eikum <coldpie@fastmail.com>
 *
 *   From <linux/drivers/hid/hid-lg4ff.c>:
 * Copyright (c) 2010 Simon Wood <simon@mungewell.org>
 *
 *   From <linux/drivers/hid/hid-lg.c>:
 * Copyright (c) 1999 Andreas Gal
 * Copyright (c) 2000-2005 Vojtech Pavlik <vojtech@suse.cz>
 * Copyright (c) 2005 Michael Haboustak <mike-@cinci.rr.com> for Concept2, Inc
 * Copyright (c) 2006-2007 Jiri Kosina
 * Copyright (c) 2008 Jiri Slaby
 * Copyright (c) 2010 Hendrik Iben
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <IOKit/IOLocks.h>
#include <IOKit/usb/IOUSBHostHIDDevice.h>
#include <IOKit/usb/IOUSBHostDevice.h>
#include <IOKit/usb/IOUSBHostInterface.h>
#include <IOKit/usb/IOUSBHostPipe.h>

class com_smokingonabike_driver_logiwheel2000;

struct lg4ff_wheel_data {
    uint32_t product_id;
    uint16_t range;
    uint16_t min_range;
    uint16_t max_range;
#ifdef CONFIG_LEDS_CLASS
    uint8_t  led_state;
    struct led_classdev *led[5];
#endif
    uint32_t alternate_modes;
    const char * real_tag;
    const char * real_name;
    uint16_t real_product_id;

    void (*set_range)(com_smokingonabike_driver_logiwheel2000 *hid, uint16_t range);
};

class com_smokingonabike_driver_logiwheel2000 : public IOUSBHostHIDDevice
{
    OSDeclareDefaultStructors(com_smokingonabike_driver_logiwheel2000)

    public:
        virtual bool start(IOService *provider);

        struct lg4ff_wheel_data wdata;
        uint8_t ffbit;
        void (*set_autocenter)(com_smokingonabike_driver_logiwheel2000 *hid, uint16_t magnitude);
};
