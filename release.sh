#!/bin/bash 

rm -rf build/ release/

xcrun xcodebuild -configuration Release -target "logiwheel2000"

mkdir release
mv build/Release/logiwheel2000.kext release/

hdiutil create -srcfolder release/ -format UDZO logiwheel2000-$(date '+%Y-%m-%d').dmg
