#include <ForceFeedback/IOForceFeedbackLib.h>
#include <IOKit/IOCFPlugin.h>
#include <IOKit/hid/IOHIDLib.h>

#define PLUGIN_ID "8D3148F7-6A0E-4DD3-9B57-C87CD3ED4693"

#define LOGPFX "logiwheel2000fb: "

#define MAX_EFFECTS 64

static FILE *f = NULL;

static IOReturn set_report(IOHIDDeviceInterface121 **hid, const unsigned char *data, uint32_t data_len)
{
    IOReturn err;

    err = (*hid)->setReport(hid, kIOHIDReportTypeOutput, 0, (void*)data, data_len, 10000, NULL, NULL, NULL);
    if(err != kIOReturnSuccess)
        fprintf(f, LOGPFX "setReport failed: %x\n", err);

    return err;
}

struct CONSTANT_EFFECT {
    bool used, started;
    FFEFFECT ffeffect;
    FFCONSTANTFORCE constant;
};

typedef struct __logiwheel2000fbPluginType
{
    const IOForceFeedbackDeviceInterface *IOForceFeedbackDeviceInterface_iface;
    const IOCFPlugInInterface *IOCFPlugInInterface_iface;
    CFUUIDRef    factoryID;
    UInt32       refCount;
    unsigned int idx;
    struct CONSTANT_EFFECT effects[MAX_EFFECTS];
    IOHIDDeviceInterface121 **device;
} logiwheel2000fbPluginType;

#define CONTAINING_RECORD(address, type, field) \
      ((type *)((char *)(address) - offsetof(type, field)))

static inline logiwheel2000fbPluginType *impl_from_IOForceFeedbackDeviceInterface(IOForceFeedbackDeviceInterface *iface)
{
    return CONTAINING_RECORD(iface, logiwheel2000fbPluginType, IOForceFeedbackDeviceInterface_iface);
}

static inline logiwheel2000fbPluginType *impl_from_IOCFPlugInInterface(IOCFPlugInInterface *iface)
{
    return CONTAINING_RECORD(iface, logiwheel2000fbPluginType, IOCFPlugInInterface_iface);
}

static HRESULT logiwheel2000fb_QueryInterface(void *self, REFIID iid,
        void **ppv)
{
    CFUUIDRef interfaceID;
    logiwheel2000fbPluginType *This = impl_from_IOForceFeedbackDeviceInterface(self);

    interfaceID = CFUUIDCreateFromUUIDBytes(kCFAllocatorDefault, iid);

    if (CFEqual(interfaceID, kIOForceFeedbackDeviceInterfaceID)){
        CFRelease(interfaceID);
        This->IOForceFeedbackDeviceInterface_iface->AddRef(&This->IOForceFeedbackDeviceInterface_iface);
        *ppv = &This->IOForceFeedbackDeviceInterface_iface;
#if DEBUG
        fprintf(f, LOGPFX "Requested FFB interface, This: %p, ppv: %p\n", This, *ppv);
#endif
        return S_OK;
    }else if (CFEqual(interfaceID, IUnknownUUID) || CFEqual(interfaceID, kIOCFPlugInInterfaceID)){
        CFRelease(interfaceID);
        This->IOForceFeedbackDeviceInterface_iface->AddRef(&This->IOForceFeedbackDeviceInterface_iface);
        *ppv = &This->IOCFPlugInInterface_iface;
#if DEBUG
        fprintf(f, LOGPFX "Requested kIOCFPlugInInterfaceID, This: %p, ppv: %p\n", This, *ppv);
#endif
        return S_OK;
    }

#if DEBUG
    fprintf(f, LOGPFX "Requested unknown interface\n");
#endif
    *ppv = NULL;
    CFRelease(interfaceID);

    return E_NOINTERFACE;
}

static ULONG logiwheel2000fb_PluginAddRef(void *self)
{
#if DEBUG
    fprintf(f, LOGPFX "AddRef\n");
#endif
    logiwheel2000fbPluginType *This = impl_from_IOForceFeedbackDeviceInterface(self);
    This->refCount += 1;
    return This->refCount;
}

static ULONG logiwheel2000fb_PluginRelease(void *self)
{
#if DEBUG
    fprintf(f, LOGPFX "Release\n");
#endif
    logiwheel2000fbPluginType *This = impl_from_IOForceFeedbackDeviceInterface(self);
    This->refCount -= 1;

    if (This->refCount == 0){
        if (This->factoryID){
            CFPlugInRemoveInstanceForFactory(This->factoryID);
            CFRelease(This->factoryID);
        }
        free(This);
        return 0;
    }

    return This->refCount;
}

static HRESULT logiwheel2000fb_ForceFeedbackGetVersion(void *self,
        ForceFeedbackVersion *version)
{
#if DEBUG
    fprintf(f, LOGPFX "GetVersion\n");
#endif
    version->apiVersion.majorRev = kFFPlugInAPIMajorRev;
    version->apiVersion.minorAndBugRev = kFFPlugInAPIMinorAndBugRev;
    version->apiVersion.stage = kFFPlugInAPIStage;
    version->apiVersion.nonRelRev = kFFPlugInAPINonRelRev;
    version->plugInVersion.majorRev = 1;
    version->plugInVersion.minorAndBugRev = 0;
    version->plugInVersion.stage = 0;
    version->plugInVersion.nonRelRev = 0;
    return S_OK;
} 
 
static HRESULT logiwheel2000fb_InitializeTerminate(void *self,
        NumVersion forceFeedbackAPIVersion, io_object_t hidDevice,
        boolean_t begin)
{
    logiwheel2000fbPluginType *This = impl_from_IOForceFeedbackDeviceInterface(self);
    IOReturn err;

#if DEBUG
    fprintf(f, LOGPFX "initialize terminate\n");
#endif

    if(begin){
        IOCFPlugInInterface **pif;
        SInt32 score;

        err = IOCreatePlugInInterfaceForService(hidDevice, kIOHIDDeviceUserClientTypeID, kIOCFPlugInInterfaceID, &pif, &score);
        if(err != kIOReturnSuccess){
            fprintf(f, LOGPFX "IOCreatePlugInInterfaceForService failed: %x\n", err);
            return E_FAIL;
        }

        err = (*pif)->QueryInterface(pif, CFUUIDGetUUIDBytes(kIOHIDDeviceInterfaceID121), (void**)&This->device);
        if(err != kIOReturnSuccess){
            fprintf(f, LOGPFX "QueryInterface(IOHIDDeviceInterfaceID121) failed: %x\n", err);
            (*pif)->Release(pif);
            return E_FAIL;
        }

        (*This->device)->open(This->device, 0);

        (*pif)->Release(pif);
    }else{
        (*This->device)->close(This->device);
        (*This->device)->Release(This->device);
        (*This->device) = NULL;
    }

    return S_OK;
} 
 
static HRESULT logiwheel2000fb_DestroyEffect(void *self,
        FFEffectDownloadID downloadID)
{
    logiwheel2000fbPluginType *This = impl_from_IOForceFeedbackDeviceInterface(self);

#if DEBUG
    fprintf(f, LOGPFX "DestroyEffect\n");
#endif

    This->effects[downloadID].used = false;

    return S_OK;
} 
 
static HRESULT logiwheel2000fb_DownloadEffect(void *self, CFUUIDRef effectType,
        FFEffectDownloadID *pDownloadID, FFEFFECT *pEffect,
        FFEffectParameterFlag flags)
{
    logiwheel2000fbPluginType *This = impl_from_IOForceFeedbackDeviceInterface(self);
    int i, idx;

#if DEBUG
    fprintf(f, LOGPFX "DownloadEffect\n");
#endif

    if(*pDownloadID){
        idx = *pDownloadID;
    }else{
        for(i = 0; i < MAX_EFFECTS; ++i){
            if(!This->effects[(This->idx + i) % MAX_EFFECTS].used)
                break;
        }
        if(i == MAX_EFFECTS)
            return FFERR_DEVICEFULL;

        idx = (This->idx + i) % MAX_EFFECTS;
        This->idx = (idx + 1) % MAX_EFFECTS;
    }


    This->effects[idx].used = true;
    This->effects[idx].ffeffect = *pEffect;
    This->effects[idx].ffeffect.lpvTypeSpecificParams = &This->effects[idx].constant;
    This->effects[idx].constant.lMagnitude = ((FFCONSTANTFORCE*)pEffect->lpvTypeSpecificParams)->lMagnitude;

    if(*pDownloadID){
        /* FIXME this could be more elegant */
        if(This->effects[idx].started){
            This->IOForceFeedbackDeviceInterface_iface->StartEffect(self,
                    *pDownloadID, 0, 0);
        }
    }

    *pDownloadID = idx;

    return S_OK;
} 
 
static HRESULT logiwheel2000fb_Escape(void *self,
        FFEffectDownloadID downloadID, FFEFFESCAPE *pEscape)
{
#if DEBUG
    fprintf(f, LOGPFX "Escape\n");
#endif
    return S_OK;
} 
 
static HRESULT logiwheel2000fb_GetEffectStatus(void *self,
        FFEffectDownloadID downloadID, FFEffectStatusFlag *pStatusCode)
{
#if DEBUG
    fprintf(f, LOGPFX "GetEffectStatus\n");
#endif
    return S_OK;
} 
 
static HRESULT logiwheel2000fb_GetForceFeedbackCapabilities(void *self,
        FFCAPABILITIES *pCapabilities)
{
#if DEBUG
    fprintf(f, LOGPFX "GetFFBCapabilities\n");
#endif
    memset(pCapabilities, 0, sizeof(*pCapabilities));

    pCapabilities->ffSpecVer.majorRev = kFFPlugInAPIMajorRev;
    pCapabilities->ffSpecVer.minorAndBugRev = kFFPlugInAPIMinorAndBugRev;
    pCapabilities->ffSpecVer.stage = kFFPlugInAPIStage;
    pCapabilities->ffSpecVer.nonRelRev = kFFPlugInAPINonRelRev;
    pCapabilities->supportedEffects = FFCAP_ET_CONSTANTFORCE;
    pCapabilities->emulatedEffects = 0;
    pCapabilities->subType = FFCAP_ST_KINESTHETIC;
    pCapabilities->numFfAxes = 1;
    pCapabilities->ffAxes[0] = FFJOFS_X;
    pCapabilities->storageCapacity = MAX_EFFECTS;
    pCapabilities->playbackCapacity = 1;
    pCapabilities->driverVer.majorRev = 1;
    pCapabilities->driverVer.minorAndBugRev = 0;
    pCapabilities->driverVer.stage = 0;
    pCapabilities->driverVer.nonRelRev = 0;
    pCapabilities->firmwareVer.majorRev = 1;
    pCapabilities->firmwareVer.minorAndBugRev = 0;
    pCapabilities->firmwareVer.stage = developStage;
    pCapabilities->firmwareVer.nonRelRev = 0;
    pCapabilities->hardwareVer.majorRev = 1;
    pCapabilities->hardwareVer.minorAndBugRev = 0;
    pCapabilities->hardwareVer.stage = developStage;
    pCapabilities->hardwareVer.nonRelRev = 0;

    return S_OK;
} 
 
static HRESULT logiwheel2000fb_GetForceFeedbackState(void *self,
        ForceFeedbackDeviceState *pDeviceState)
{
#if DEBUG
    fprintf(f, LOGPFX "GetFFBState\n");
#endif
    return S_OK;
} 
 
static HRESULT logiwheel2000fb_SendForceFeedbackCommand(void *self,
        FFCommandFlag state)
{
#if DEBUG
    fprintf(f, LOGPFX "SendFFBCmd: 0x%x\n", state);
#endif
    return S_OK;
} 
 
static HRESULT logiwheel2000fb_SetProperty(void *self, FFProperty property,
        void *pValue)
{
#if DEBUG
    fprintf(f, LOGPFX "SetProperty: 0x%x\n", property);
#endif
    return S_OK;
} 
 
static HRESULT logiwheel2000fb_StartEffect(void *self,
        FFEffectDownloadID downloadID, FFEffectStartFlag mode,
        UInt32 iterations)
{
    logiwheel2000fbPluginType *This = impl_from_IOForceFeedbackDeviceInterface(self);
    unsigned char data[7];

#if DEBUG
    fprintf(f, LOGPFX "StartEffect\n");
#endif

    if(!This->effects[downloadID].used)
        return FFERR_INVALIDDOWNLOADID;

    This->effects[downloadID].started = true;

    if(This->effects[downloadID].constant.lMagnitude == 0x0){
        /* disable effect */
        data[0] = 0x13;
        data[1] = 0x00;
        data[2] = 0x00;
        data[3] = 0x00;
        data[4] = 0x00;
        data[5] = 0x00;
        data[6] = 0x00;

        set_report(This->device, data, sizeof(data));

        return S_OK;
    }

    double r = (This->effects[downloadID].constant.lMagnitude + 10000) / 20000.;
    unsigned char x = 0xFF * r;

#if DEBUG
    fprintf(f, LOGPFX "magnitude %d converted to 0x%02x\n", This->effects[downloadID].constant.lMagnitude, (unsigned int)x);
#endif
    data[0] = 0x11;
    data[1] = 0x08;
    data[2] = (unsigned char)x;
    data[3] = 0x80;
    data[4] = 0x00;
    data[5] = 0x00;
    data[6] = 0x00;

    set_report(This->device, data, sizeof(data));

    return S_OK;
} 
 
static HRESULT logiwheel2000fb_StopEffect(void *self,
        FFEffectDownloadID downloadID)
{
    logiwheel2000fbPluginType *This = impl_from_IOForceFeedbackDeviceInterface(self);
    unsigned char data[7];

#if DEBUG
    fprintf(f, LOGPFX "StopEffect\n");
#endif

    if(!This->effects[downloadID].used)
        return FFERR_INVALIDDOWNLOADID;

    This->effects[downloadID].started = false;

    /* disable effect */
    data[0] = 0x13;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x00;
    data[4] = 0x00;
    data[5] = 0x00;
    data[6] = 0x00;

    set_report(This->device, data, sizeof(data));

    return S_OK;
}

static IOForceFeedbackDeviceInterface logiwheel2000fb_vtbl = {
    NULL, /* reserved by IUNKNOWN_C_GUTS */
    logiwheel2000fb_QueryInterface,
    logiwheel2000fb_PluginAddRef,
    logiwheel2000fb_PluginRelease,
    logiwheel2000fb_ForceFeedbackGetVersion,
    logiwheel2000fb_InitializeTerminate,
    logiwheel2000fb_DestroyEffect,
    logiwheel2000fb_DownloadEffect,
    logiwheel2000fb_Escape,
    logiwheel2000fb_GetEffectStatus,
    logiwheel2000fb_GetForceFeedbackCapabilities,
    logiwheel2000fb_GetForceFeedbackState,
    logiwheel2000fb_SendForceFeedbackCommand,
    logiwheel2000fb_SetProperty,
    logiwheel2000fb_StartEffect,
    logiwheel2000fb_StopEffect,
};

static HRESULT logiwheel2000fb_plugin_QueryInterface(void *self, REFIID iid,
        void **ppv)
{
    logiwheel2000fbPluginType *This = impl_from_IOCFPlugInInterface(self);
    return This->IOForceFeedbackDeviceInterface_iface->QueryInterface(
            &This->IOForceFeedbackDeviceInterface_iface,
            iid, ppv);
}

static ULONG logiwheel2000fb_plugin_AddRef(void *self)
{
    logiwheel2000fbPluginType *This = impl_from_IOCFPlugInInterface(self);
    return This->IOForceFeedbackDeviceInterface_iface->AddRef(
            &This->IOForceFeedbackDeviceInterface_iface);
}

static ULONG logiwheel2000fb_plugin_Release(void *self)
{
    logiwheel2000fbPluginType *This = impl_from_IOCFPlugInInterface(self);
    return This->IOForceFeedbackDeviceInterface_iface->Release(
            &This->IOForceFeedbackDeviceInterface_iface);
}

static IOReturn logiwheel2000fb_plugin_Probe(void *self,
        CFDictionaryRef propertyTable, io_service_t service, SInt32 *order)
{
#if DEBUG
    fprintf(f, LOGPFX "Probe\n");
#endif
    if(service && IOObjectConformsTo(service, "com_smokingonabike_driver_logiwheel2000"))
        return kIOReturnSuccess;
    fprintf(f, LOGPFX "Probe: Invalid service?\n");
    return kIOReturnBadArgument;
}

static IOReturn logiwheel2000fb_plugin_Start(void *self,
        CFDictionaryRef propertyTable, io_service_t service)
{
#if DEBUG
    fprintf(f, LOGPFX "Start\n");
#endif
    return kIOReturnSuccess;
}

static IOReturn logiwheel2000fb_plugin_Stop(void *self)
{
#if DEBUG
    fprintf(f, LOGPFX "Stop\n");
#endif
    return kIOReturnSuccess;
}

static IOCFPlugInInterface logiwheel2000fb_plugin_vtbl = {
    NULL,
    logiwheel2000fb_plugin_QueryInterface,
    logiwheel2000fb_plugin_AddRef,
    logiwheel2000fb_plugin_Release,
    1, 0, /* version, revision, */
    logiwheel2000fb_plugin_Probe,
    logiwheel2000fb_plugin_Start,
    logiwheel2000fb_plugin_Stop,
};

logiwheel2000fbPluginType *Alloclogiwheel2000fbPluginType(CFUUIDRef inFactoryID)
{
    logiwheel2000fbPluginType *theNewInstance;

    theNewInstance = (logiwheel2000fbPluginType *)malloc(sizeof(logiwheel2000fbPluginType));
    memset(theNewInstance,0,sizeof(logiwheel2000fbPluginType));

    theNewInstance->IOForceFeedbackDeviceInterface_iface = &logiwheel2000fb_vtbl;
    theNewInstance->IOCFPlugInInterface_iface = &logiwheel2000fb_plugin_vtbl;

    theNewInstance->factoryID = CFRetain(inFactoryID);
    CFPlugInAddInstanceForFactory(inFactoryID);

    theNewInstance->refCount = 1;
    return theNewInstance;
}

void *logiwheel2000fbPluginFactory(CFAllocatorRef allocator, CFUUIDRef typeID)
{
    logiwheel2000fbPluginType *result;
    CFUUIDRef uuid;

#if 0
    if(!f){
        f = fopen("/tmp/logiwheel2000fb.txt", "w");
        if(!f){
            fprintf(stderr, "logiwheel2000fb: opening </tmp/logiwheel2000fb.txt> failed\n");
            f = stderr;
        }
    }
#endif
    f = stderr;

    if (CFEqual(typeID, kIOForceFeedbackLibTypeID)){
#if DEBUG
        fprintf(f, LOGPFX "Allocating a new factory.\n");
#endif
        uuid = CFUUIDCreateFromString(kCFAllocatorDefault, CFSTR(PLUGIN_ID));
        result = Alloclogiwheel2000fbPluginType(uuid);
        CFRelease(uuid);
#if DEBUG
        fprintf(f, LOGPFX "Returning %p\n", &result->IOCFPlugInInterface_iface);
#endif
        return &result->IOCFPlugInInterface_iface;
    }

    fprintf(f, LOGPFX "Did not recognize requested type\n");

    return NULL;
}

