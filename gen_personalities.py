#!/usr/bin/env python

import sys

with open(sys.argv[1]) as f:
    for line in f:
        strs = line.split(" ")
        dec = int(strs[2].strip(), 16)
        print "\t\t<key>logiwheel2000_" + strs[0] + "</key>"
        print "\t\t<dict>"
        print "\t\t\t<key>IOProviderClass</key>"
        print "\t\t\t<string>IOUSBHostInterface</string>"
        print "\t\t\t<key>IOClass</key>"
        print "\t\t\t<string>com_smokingonabike_driver_logiwheel2000</string>"
        print "\t\t\t<key>CFBundleIdentifier</key>"
        print "\t\t\t<string>$(PRODUCT_BUNDLE_IDENTIFIER)</string>"
        print "\t\t\t<key>bConfigurationValue</key>"
        print "\t\t\t<integer>1</integer>"
        print "\t\t\t<key>bInterfaceNumber</key>"
        print "\t\t\t<integer>0</integer>"
        print "\t\t\t<key>idVendor</key>"
        print "\t\t\t<integer>1133</integer> <!-- == 0x046d USB_VENDOR_ID_LOGITECH -->"
        print "\t\t\t<key>idProduct</key>"
        print "\t\t\t<integer>" + str(dec) +  "</integer> <!-- == 0x" + strs[2].strip() + " " + strs[1] + " -->"
        print "\t\t\t<key>IOCFPlugInTypes</key>"
        print "\t\t\t<dict>"
        print "\t\t\t\t<key>F4545CE5-BF5B-11D6-A4BB-0003933E3E3E</key> <!-- kIOForceFeedbackLibTypeID -->"
        print "\t\t\t\t<string>logiwheel2000.kext/Contents/PlugIns/logiwheel2000fb.plugin</string>"
        print "\t\t\t</dict>"
        print "\t\t</dict>"

